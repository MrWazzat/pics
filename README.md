# CI / CD

## Etape 0 : Modifications de la structure

J'ai commencé par ajouter les branches `develop` et `release`. 

J'ai aussi ajouté de quoi faire le changelog, en utilisant `gitmoji-changelog`.

## Etape 1 : Déploiement sur Heroku

Pour cette partie, j'ai utilisé les tutoriels / ressources suivantes :
    
* [Intégration Continue/Déploiement Continue (CI/CD) avec Gitlab et Heroku](https://medium.com/@JohanPujol/int%C3%A9gration-continue-d%C3%A9ploiement-continue-ci-cd-avec-gitlab-et-heroku-b809801a1524)
* [How do I deploy my code to Heroku using GitLab CI/CD?](https://medium.com/swlh/how-do-i-deploy-my-code-to-heroku-using-gitlab-ci-cd-6a232b6be2e4)

### Mise en place sur Heroku 

* Création de 3 apps, qui correspondent à l'environnement de développement, recette et production (respectivement les branches develop, release et master). 
* Récupération de la clé d'API permettant de faire le pont avec GitLab CI. 
* Création des variables dans GitLab *(Settings/CI-CD)* qui seront utilisés dans le fichier `gitlab-ci.yml`.

### Création du fichier gitlab-ci.yml

* Création du fichier de configuration de l'intégration continue pour les 3 environnements. 

