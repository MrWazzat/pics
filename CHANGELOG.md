# Changelog

<a name="0.1.0"></a>
## 0.1.0 (2020-03-12)

### Added

- 🎉 Initial Commit [[a0f04a1](https://gitlab.com/MrWazzat/pics/commit/a0f04a16d43c48c0ced9a2b8ee0b2b305bced3b4)]


